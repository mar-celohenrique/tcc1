package br.ufc.quixada.tcc1.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ufc.quixada.tcc1.activity.MainActivity;
import br.ufc.quixada.tcc1.activity.SplashActivity;
import br.ufc.quixada.tcc1.model.DenunciaSinistro;
import br.ufc.quixada.tcc1.service.DenunciaSinistroService;
import br.ufc.quixada.tcc1.util.CoplavUtils;

/**
 * Created by marcelohenrique on 27/11/17.
 */

public class PopularBaseDeDadosTask extends AsyncTask<String, Void, List<DenunciaSinistro>> {

    private static final String ATUALIZACAO = "atualizacao";
    private List<DenunciaSinistro> lista;
    private Context context;
    private DenunciaSinistroService service;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private Intent intent;

    public PopularBaseDeDadosTask(DenunciaSinistroService service, Context context) {

        if (null == service) {
            throw new IllegalArgumentException("O service não pode ser nulo");
        } else if (null == context) {
            throw new IllegalArgumentException("O context não pode ser nulo");
        }

        this.service = service;
        this.context = context;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        intent = new Intent(context, MainActivity.class);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(context, "Aguarde!",
                "Carregando...");
    }

    @Override
    protected void onPostExecute(List<DenunciaSinistro> denunciaSinistros) {
        if (denunciaSinistros.size() > 0) {
            CoplavUtils.salvarDados(denunciaSinistros, context);
            Date date = new Date(System.currentTimeMillis());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(ATUALIZACAO, date.getTime());
            editor.commit();
            progressDialog.dismiss();
            ((SplashActivity) context).finish();
            context.startActivity(intent);

        } else {
            progressDialog.dismiss();
            Toast.makeText(context, "Não foi possível conectar ao servidor, verifique sua conexão.", Toast.LENGTH_SHORT).show();
            ((SplashActivity) context).finish();
            context.startActivity(intent);

        }
    }

    @Override
    protected List<DenunciaSinistro> doInBackground(String... strings) {
        try {
            return service.buscarSinistros();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
