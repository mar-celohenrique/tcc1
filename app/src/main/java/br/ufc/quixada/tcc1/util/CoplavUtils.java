package br.ufc.quixada.tcc1.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.widget.EditText;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.ufc.quixada.tcc1.model.DenunciaSinistro;
import io.realm.Realm;

/**
 * Created by marcelohenrique on 27/11/17.
 */

public class CoplavUtils {

    public static void adicionarMascara(EditText editText) {

        if (null == editText) {
            throw new NullPointerException("O campo de texto não pode ser nulo");
        }

        SimpleMaskFormatter smf = new SimpleMaskFormatter("LLL-NNNN");
        MaskTextWatcher mtw = new MaskTextWatcher(editText, smf);
        editText.addTextChangedListener(mtw);
    }

    public static void validarChassi(String chassi) {
        if (null != chassi) {
            if (chassi.trim().length() != 17) {
                throw new IllegalArgumentException("O chassi deve conter 17 caracteres");
            }

            Pattern zeroNoPrimeiroDigito = Pattern.compile("^0");
            Matcher matcherZero = zeroNoPrimeiroDigito.matcher(chassi);

            Pattern espacoNoChassi = Pattern.compile(" ");
            Matcher matcherEspaco = espacoNoChassi.matcher(chassi);

            Pattern repeticaoMaisDe6Vezes = Pattern.compile("^.{4,}([0-9A-Z])\\1{5,}");
            Matcher matcherRepetir = repeticaoMaisDe6Vezes.matcher(chassi);

            Pattern caracteresiIoOqQ = Pattern.compile("[iIoOqQ]");
            Matcher matcherCaract = caracteresiIoOqQ.matcher(chassi);

            Pattern ultimos6Numericos = Pattern.compile("[0-9]{6}$");
            Matcher matcherUltimos = ultimos6Numericos.matcher(chassi);

            if (matcherZero.find() || matcherEspaco.find() || matcherRepetir.find() || matcherCaract.find()
                    || !matcherUltimos.find()) {
                throw new IllegalArgumentException("O número do chassi é inválido");
            }
        } else {
            throw new IllegalArgumentException("O número do chassi não pode ser nulo");
        }
    }

    public static void salvarDados(final List<DenunciaSinistro> denunciaSinistros, Context context) {

        if (null == denunciaSinistros || denunciaSinistros.isEmpty()) {
            throw new IllegalArgumentException("Devem existir dados a serem persistidos");
        }
        if (null == Realm.getDefaultInstance()) {
            Realm.init(context);
        }
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(denunciaSinistros);
            }
        });
    }


    public static LatLng buscarLocalizacaoPeloEndereco(String endereco, Geocoder geocoder) {

        if (null == endereco || endereco.replaceAll(" ", "").length() == 0 || endereco.replaceAll(" ", "").length() <= 3) {
            throw new IllegalArgumentException("Deve haver um endereço válido a ser buscado");
        }
        List<Address> address;
        LatLng resLatLng = null;

        try {
            // May throw an IOException
            address = geocoder.getFromLocationName(endereco, 5);
            if (address == null) {
                return null;
            }

            if (address.size() == 0) {
                return null;
            }

            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            resLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return resLatLng;
    }

}
