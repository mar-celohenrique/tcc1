package br.ufc.quixada.tcc1.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import br.ufc.quixada.tcc1.R;
import br.ufc.quixada.tcc1.fragment.BuscaImagemFragment;
import br.ufc.quixada.tcc1.fragment.BuscaManualFragment;
import br.ufc.quixada.tcc1.fragment.InformarSinistroFragment;
import br.ufc.quixada.tcc1.fragment.MapaFragment;
import br.ufc.quixada.tcc1.model.Foto;
import br.ufc.quixada.tcc1.service.RetornoPlacaService;
import br.ufc.quixada.tcc1.tasks.BuscaRetornoPlacaTask;

public class MainActivity extends AppCompatActivity {


    private BuscaImagemFragment buscaImagemFragment;

    private Uri mCropImageUri;

    private Foto foto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buscaImagemFragment = new BuscaImagemFragment();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mostrarFragment(new BuscaManualFragment());
    }

    private void mostrarFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_fragment_containers, fragment).commitNowAllowingStateLoss();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_busca_manual:
                    mostrarFragment(new BuscaManualFragment());
                    return true;
                case R.id.navigation_busca_foto:
                    mostrarFragment(buscaImagemFragment);
                    return true;
                case R.id.navigation_informar_sinistro:
                    mostrarFragment(new InformarSinistroFragment());
                    return true;
                case R.id.navigation_mapa:
                    mostrarFragment(new MapaFragment());
                    return true;
            }
            return false;
        }

    };

    private void iniciarCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setActivityTitle("Ajustar imagem")
                .setBorderLineColor(Color.CYAN)
                .setBorderCornerColor(R.color.colorAccent)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setFixAspectRatio(true)
                .start(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {



        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                iniciarCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                File file;

                try {
                    file = new File(imageUri.getPath());


                    String base64 = Base64.encodeToString(FileUtils.readFileToByteArray(file), Base64.DEFAULT);
                    this.foto = new Foto();
                    this.foto.setBase64(base64);
                    this.foto.setNome(file.getName());

                    RetornoPlacaService retornoPlacaService = new RetornoPlacaService(getResources().getString(R.string.servidor));
                    BuscaRetornoPlacaTask buscaRetornoPlacaTask = new BuscaRetornoPlacaTask(this, retornoPlacaService, foto, null, false);
                    buscaRetornoPlacaTask.execute();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {

                if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Toast.makeText(this, "Erro", Toast.LENGTH_LONG).show();
                    Log.i("Cropping Image", "Cropping failed: " + result.getError());
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            iniciarCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }
}
