package br.ufc.quixada.tcc1.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.ufc.quixada.tcc1.R;
import br.ufc.quixada.tcc1.model.DenunciaSinistro;
import br.ufc.quixada.tcc1.service.DenunciaSinistroService;
import br.ufc.quixada.tcc1.tasks.InformarSinistroTask;
import br.ufc.quixada.tcc1.util.CoplavUtils;
import mehdi.sakout.fancybuttons.FancyButton;

public class InformarSinistroFragment extends Fragment implements View.OnClickListener {

    private static EditText dataSinistro;
    private AutoCompleteTextView chassi;
    private AutoCompleteTextView placa;
    private AutoCompleteTextView cor;
    private AutoCompleteTextView ano;
    private AutoCompleteTextView anoModelo;
    private AutoCompleteTextView marca;
    private AutoCompleteTextView modelo;
    private AutoCompleteTextView cidade;
    private AutoCompleteTextView endereco;
    private Spinner estados;
    private Spinner tiposSinistros;
    private FancyButton btnInformarSinistro;

    public InformarSinistroFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_informar_sinistro, container, false);


        this.estados = view.findViewById(R.id.denuncia_estado);
        ArrayAdapter<CharSequence> estados = ArrayAdapter.createFromResource(getActivity(),
                R.array.estados_brasileiros, android.R.layout.simple_spinner_item);
        estados.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.estados.setAdapter(estados);

        this.tiposSinistros = view.findViewById(R.id.denuncia_tipo_sinistro);
        ArrayAdapter<CharSequence> sinistros = ArrayAdapter.createFromResource(getActivity(),
                R.array.tipos_sinistros, android.R.layout.simple_spinner_item);
        sinistros.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.tiposSinistros.setAdapter(sinistros);

        this.chassi = view.findViewById(R.id.denuncia_chassi);
        this.btnInformarSinistro = view.findViewById(R.id.btn_informar_sinistro);
        this.placa = view.findViewById(R.id.denuncia_placa);
        this.cor = view.findViewById(R.id.denuncia_cor);
        this.ano = view.findViewById(R.id.denuncia_ano);
        this.anoModelo = view.findViewById(R.id.denuncia_ano_modelo);
        this.cidade = view.findViewById(R.id.denuncia_cidade);
        this.marca = view.findViewById(R.id.denuncia_marca);
        this.modelo = view.findViewById(R.id.denuncia_modelo);
        this.endereco = view.findViewById(R.id.denuncia_endereco);
        this.btnInformarSinistro.setOnClickListener(this);
        this.dataSinistro = view.findViewById(R.id.denuncia_data_ocorrencia);
        this.dataSinistro.setOnClickListener(this);

        CoplavUtils.adicionarMascara(this.placa);

        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_informar_sinistro:
                DenunciaSinistro denunciaSinistro = new DenunciaSinistro();

                denunciaSinistro.setAno(this.ano.getText().toString().trim());
                denunciaSinistro.setAnoModelo(this.anoModelo.getText().toString().trim());
                denunciaSinistro.setPlaca(this.placa.getText().toString().trim());
                denunciaSinistro.setModelo(this.modelo.getText().toString().trim());
                denunciaSinistro.setUf(this.estados.getSelectedItem().toString().trim());
                denunciaSinistro.setCor(this.cor.getText().toString().trim());
                denunciaSinistro.setMarca(this.marca.getText().toString().trim());
                denunciaSinistro.setEndereco(this.endereco.getText().toString().trim());
                denunciaSinistro.setCidade(this.cidade.getText().toString().trim());
                denunciaSinistro.setChassi(this.chassi.getText().toString().trim().toUpperCase());
                denunciaSinistro.setDataInformado(new Date());
                denunciaSinistro.setDataSinistro(new Date(dataSinistro.getText().toString()));
                denunciaSinistro.setSituacao(this.tiposSinistros.getSelectedItem().toString().trim());
                Geocoder geocoder = new Geocoder(getActivity());
                LatLng latLng = CoplavUtils.buscarLocalizacaoPeloEndereco(this.endereco.getText().toString().trim(), geocoder);
                denunciaSinistro.setLatitude(latLng.latitude);
                denunciaSinistro.setLongitude(latLng.longitude);
                denunciaSinistro.setSituacao(this.tiposSinistros.getSelectedItem().toString());


                DenunciaSinistroService denunciaSinistroService = new DenunciaSinistroService(getResources().getString(R.string.servidor));
                InformarSinistroTask informarSinistroTask = new InformarSinistroTask(getActivity(), denunciaSinistroService, denunciaSinistro);
                informarSinistroTask.execute();

                break;
            case R.id.denuncia_data_ocorrencia:
                DialogFragment calendario = new DatePickerFragment();
                calendario.show(getActivity().getSupportFragmentManager(), "datePicker");
                break;

            default:
                break;
        }
    }

    @SuppressLint("ValidFragment")
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, month);
            c.set(Calendar.DAY_OF_MONTH, day);
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            String dataFormatada = formato.format(c.getTime());
            dataSinistro.setText(dataFormatada);
            dataSinistro.requestFocus();
        }
    }
}
