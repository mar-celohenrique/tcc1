package br.ufc.quixada.tcc1.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import br.ufc.quixada.tcc1.R;
import br.ufc.quixada.tcc1.model.DenunciaSinistro;
import br.ufc.quixada.tcc1.service.DenunciaSinistroService;
import br.ufc.quixada.tcc1.tasks.BuscarAtualizacaoSinistrosTask;
import br.ufc.quixada.tcc1.tasks.PopularBaseDeDadosTask;
import io.realm.Realm;
import io.realm.RealmResults;

public class SplashActivity extends AppCompatActivity {

    private Realm realm;
    private DenunciaSinistroService service;
    private PopularBaseDeDadosTask popularBaseDeDados;
    private BuscarAtualizacaoSinistrosTask buscarAtualizacaoBadeDeDados;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        this.service = new DenunciaSinistroService(getResources().getString(R.string.servidor));
        this.popularBaseDeDados = new PopularBaseDeDadosTask(service, this);
        this.buscarAtualizacaoBadeDeDados = new BuscarAtualizacaoSinistrosTask(service, this);
        this.realm = Realm.getDefaultInstance();
        this.handler = new Handler();
        atualizarBaseDeDados();
    }

    public void atualizarBaseDeDados() {
        RealmResults<DenunciaSinistro> marcadores = realm.where(DenunciaSinistro.class).findAll();
        if (marcadores.isEmpty()) {
            popularBaseDeDados.execute();
        } else {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    buscarAtualizacaoBadeDeDados.execute();
                }
            }, 3000);

        }
    }

}
