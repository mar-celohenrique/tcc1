package br.ufc.quixada.tcc1.model;

/**
 * Created by marcelohenrique on 20/11/17.
 */

public class Mensagem {

    private int tipo;

    private int id;

    private String nomeObjetoRemoto;

    private String nomeMetodo;

    private String argumentos;

    public Mensagem(int tipo, int id, String nomeObjetoRemoto, String nomeMetodo, String argumentos) {
        this.tipo = tipo;
        this.id = id;
        this.nomeObjetoRemoto = nomeObjetoRemoto;
        this.nomeMetodo = nomeMetodo;
        this.argumentos = argumentos;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeObjetoRemoto() {
        return nomeObjetoRemoto;
    }

    public void setNomeObjetoRemoto(String nomeObjetoRemoto) {
        this.nomeObjetoRemoto = nomeObjetoRemoto;
    }

    public String getNomeMetodo() {
        return nomeMetodo;
    }

    public void setNomeMetodo(String nomeMetodo) {
        this.nomeMetodo = nomeMetodo;
    }

    public String getArgumentos() {
        return argumentos;
    }

    public void setArgumentos(String argumentos) {
        this.argumentos = argumentos;
    }
}
