package br.ufc.quixada.tcc1.tasks;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ufc.quixada.tcc1.activity.MainActivity;
import br.ufc.quixada.tcc1.activity.SplashActivity;
import br.ufc.quixada.tcc1.model.DenunciaSinistro;
import br.ufc.quixada.tcc1.service.DenunciaSinistroService;
import br.ufc.quixada.tcc1.util.CoplavUtils;

/**
 * Created by marcelohenrique on 27/11/17.
 */

public class BuscarAtualizacaoSinistrosTask extends AsyncTask<String, Void, List<DenunciaSinistro>> {

    private static final String ATUALIZACAO = "atualizacao";
    private Context context;
    private DenunciaSinistroService service;
    private SharedPreferences sharedPreferences;
    private Intent intent;

    public BuscarAtualizacaoSinistrosTask(DenunciaSinistroService service, Context context) {
        if (null == service) {
            throw new IllegalArgumentException("O service não pode ser nulo");
        } else if (null == context) {
            throw new IllegalArgumentException("O context não pode ser nulo");
        }
        this.service = service;
        this.context = context;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.intent = new Intent(context, MainActivity.class);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected List<DenunciaSinistro> doInBackground(String... strings) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        Date ultimaAtualizacao = null;
        if (preferences.contains(this.ATUALIZACAO)) {
            ultimaAtualizacao = new Date(preferences.getLong(this.ATUALIZACAO, 0));
        }
        if (null == ultimaAtualizacao) {
            throw new IllegalArgumentException("Não há data de atualização");
        } else {
            try {
                return this.service.buscarAtualizacao(ultimaAtualizacao);
            } catch (Exception e) {
                return new ArrayList<>();
            }
        }
    }

    @Override
    protected void onPostExecute(List<DenunciaSinistro> denuncias) {
        if (denuncias.size() > 0) {
            CoplavUtils.salvarDados(denuncias, context);
            Date date = new Date(System.currentTimeMillis());
            SharedPreferences.Editor editor = this.sharedPreferences.edit();
            editor.clear();
            editor.putLong(this.ATUALIZACAO, date.getTime());
            editor.commit();
            Toast.makeText(this.context, "Novos sinistros encontrados!", Toast.LENGTH_SHORT).show();
            ((SplashActivity) this.context).finish();
            this.context.startActivity(this.intent);
        } else {
            if (context instanceof SplashActivity) {
                ((SplashActivity) this.context).finish();
                this.context.startActivity(this.intent);
            }
        }
    }

}
