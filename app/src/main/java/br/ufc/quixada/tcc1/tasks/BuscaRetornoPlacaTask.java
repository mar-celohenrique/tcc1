package br.ufc.quixada.tcc1.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeWarningDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;

import br.ufc.quixada.tcc1.R;
import br.ufc.quixada.tcc1.model.Foto;
import br.ufc.quixada.tcc1.model.RetornoPlaca;
import br.ufc.quixada.tcc1.service.RetornoPlacaService;

/**
 * Created by marcelohenrique on 30/09/17.
 */

public class BuscaRetornoPlacaTask extends AsyncTask<String, Void, RetornoPlaca> {

    private Context context;
    private RetornoPlacaService service;
    private Foto foto;
    private String placa;
    private boolean manual;
    private AwesomeProgressDialog progress;
    private AwesomeSuccessDialog semRestricao;
    private AwesomeWarningDialog comRestricao;

    public BuscaRetornoPlacaTask(Context context, RetornoPlacaService service, Foto foto, String placa, boolean manual) {
        this.context = context;
        this.service = service;
        this.foto = foto;
        this.placa = placa;
        this.manual = manual;
        this.progress = new AwesomeProgressDialog(context);
        this.semRestricao = new AwesomeSuccessDialog(context);
        this.comRestricao = new AwesomeWarningDialog(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress.setTitle("Aguarde")
                .setMessage("Realizando busca..")
                .setColoredCircle(R.color.dialogInfoBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .show();
    }

    @Override
    protected RetornoPlaca doInBackground(String... strings) {
        RetornoPlaca retornoPlaca = null;

        if (!manual) {
            retornoPlaca = service.buscaPorImagem(this.foto);
        } else {
            retornoPlaca = service.buscaManual(this.placa);
        }

        return retornoPlaca;
    }

    @Override
    protected void onPostExecute(RetornoPlaca retornoPlaca) {
        progress.hide();
        if (null != retornoPlaca && retornoPlaca.getIdentificacao().equals(RetornoPlaca.Identificacao.SUCESSO)) {
            if (retornoPlaca.getCodigoSituacao().equals(0)) {
                exibirResultadoSemRestricao(retornoPlaca);
            } else if (retornoPlaca.getCodigoSituacao().equals(1)) {
                exibirResultadoComRestricao(retornoPlaca);
            }
        } else {
            if (manual) {
                DisplayMessage("Houve um erro e não conseguimos realizar a busca");
            } else {
                exibirDialogDicaFoto();
            }
        }
    }

    public void DisplayMessage(String menssagem) {
        Toast.makeText(context, menssagem, Toast.LENGTH_LONG).show();
    }

    public void exibirResultadoSemRestricao(RetornoPlaca retornoPlaca) {
        semRestricao.setTitle(retornoPlaca.getSituacao())
                .setMessage(retornoPlaca.toString())
                .setColoredCircle(R.color.dialogSuccessBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_success, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText("OK")
                .setPositiveButtonbackgroundColor(R.color.dialogSuccessBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        semRestricao.hide();
                    }
                })
                .show();
    }

    public void exibirResultadoComRestricao(RetornoPlaca retornoPlaca) {
        comRestricao.setTitle(retornoPlaca.getSituacao())
                .setMessage(retornoPlaca.toString())
                .setColoredCircle(R.color.dialogErrorBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_warning, R.color.white)
                .setCancelable(true)
                .setButtonText("OK")
                .setButtonTextColor(R.color.white)
                .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                .setWarningButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        comRestricao.hide();
                    }
                })
                .show();
    }

    public void exibirDialogDicaFoto() {
        comRestricao.setTitle("Placa não identificada")
                .setMessage("Caro usuário, você tentou usar a identificação por foto e não conseguimos obter os caracteres da placa. Tente selecionar na tela de ajustes da imagem, de forma aproximada, a área que corresponde a placa")
                .setColoredCircle(R.color.dialogInfoBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_warning, R.color.white)
                .setCancelable(true)
                .setButtonText("OK")
                .setButtonTextColor(R.color.white)
                .setButtonBackgroundColor(R.color.dialogInfoBackgroundColor)
                .setWarningButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        comRestricao.hide();
                    }
                })
                .show();
    }
}
