package br.ufc.quixada.tcc1.service;

import com.goebl.david.Webb;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import br.ufc.quixada.tcc1.model.Foto;
import br.ufc.quixada.tcc1.model.RetornoPlaca;

/**
 * Created by marcelohenrique on 30/09/17.
 */

public class RetornoPlacaService {


    private String url;
    private Webb webb;
    private Gson gson;

    public RetornoPlacaService(String url) {
        if (null == url) {
            throw new IllegalArgumentException("A url do servidor não pode ser nula");
        } else if (url.replaceAll(" ", "").length() == 0) {
            throw new IllegalArgumentException("A url do servidor não pode ser vazia");
        }
        this.url = url.concat("api/");
        webb = Webb.create();
        gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();
    }


    public RetornoPlaca buscaPorImagem(Foto foto) {

        if (null == foto) {
            throw new IllegalArgumentException("A foto não pode ser nula");
        } else if (null == foto.getBase64() || null == foto.getNome() || foto
                .getBase64().replaceAll(" ", "").length() <= 0 || foto.getNome().replaceAll(" ", "").length() <= 0) {
            throw new IllegalArgumentException("A foto deve conter nome e base64");
        }

        JSONObject object = webb.post(url.concat("foto"))
                .header("Content-Type", "application/json")
                .body(gson.toJson(foto))
                .useCaches(false)
                .asJsonObject()
                .getBody();

        return gson.fromJson(object.toString(), RetornoPlaca.class);

    }

    public RetornoPlaca buscaManual(String placa) {
        if (null == placa) {
            throw new IllegalArgumentException("A placa não deve ser nula");
        } else if (placa.replaceAll(" ", "").length() != 7) {
            throw new IllegalArgumentException("A placa deve conter 7 caracteres");
        }
        JSONObject object = webb.post(url.concat("manual"))
                .header("Content-Type", "application/json")
                .body(placa)
                .useCaches(false)
                .asJsonObject()
                .getBody();

        return gson.fromJson(object.toString(), RetornoPlaca.class);

    }


}
