package br.ufc.quixada.tcc1.service;


import com.goebl.david.Webb;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ufc.quixada.tcc1.model.DenunciaSinistro;
import br.ufc.quixada.tcc1.util.CoplavUtils;

/**
 * Created by marcelohenrique on 14/11/17.
 */

public class DenunciaSinistroService {


    private String url;
    private Webb webb;
    private Gson gson;

    public DenunciaSinistroService(String url) {
        if (null == url) {
            throw new IllegalArgumentException("A url do servidor não pode ser nula");
        } else if (url.replaceAll(" ", "").length() == 0) {
            throw new IllegalArgumentException("A url do servidor não pode ser vazia");
        }
        this.url = url.concat("api/");
        webb = Webb.create();
        gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();
    }

    public DenunciaSinistro denunciar(DenunciaSinistro denuncia) {


        if (null == denuncia.getPlaca()) {
            throw new IllegalArgumentException("A placa não deve ser nula");
        } else if (denuncia.getPlaca().length() != 7) {
            throw new IllegalArgumentException("A placa deve conter 7 caracteres");
        }

        try {
            CoplavUtils.validarChassi(denuncia.getChassi());
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject object = webb.post(url.concat("denunciar"))
                .header("Content-Type", "application/json")
                .body(gson.toJson(denuncia))
                .useCaches(false)
                .asJsonObject()
                .getBody();
        return gson.fromJson(object.toString(), DenunciaSinistro.class);
    }

    public List<DenunciaSinistro> buscarAtualizacao(Date data) {

        if (null == data) {
            throw new IllegalArgumentException("A data não pode ser nula");
        }

        JSONArray array = webb.post(url.concat("atualizacao"))
                .header("Content-Type", "application/json")
                .useCaches(false)
                .body(gson.toJson(data.getTime()))
                .ensureSuccess()
                .asJsonArray()
                .getBody();
        return gson.fromJson(array.toString(), new TypeToken<ArrayList<DenunciaSinistro>>() {
        }.getType());
    }

    public List<DenunciaSinistro> buscarSinistros() {
        JSONArray array = webb.get(url.concat("sinistros"))
                .header("Content-Type", "application/json")
                .useCaches(false)
                .ensureSuccess()
                .asJsonArray()
                .getBody();
        return gson.fromJson(array.toString(), new TypeToken<ArrayList<DenunciaSinistro>>() {
        }.getType());
    }

}
