package br.ufc.quixada.tcc1.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.ufc.quixada.tcc1.R;
import br.ufc.quixada.tcc1.service.RetornoPlacaService;
import br.ufc.quixada.tcc1.tasks.BuscaRetornoPlacaTask;
import br.ufc.quixada.tcc1.util.CoplavUtils;
import mehdi.sakout.fancybuttons.FancyButton;

public class BuscaManualFragment extends Fragment implements View.OnClickListener {

    private FancyButton botaoBuscaManual;
    private EditText campoPlaca;

    public BuscaManualFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_busca_manual, container, false);

        campoPlaca = rootView.findViewById(R.id.busca_manual);
        campoPlaca.setHint("ABC-1234");
        CoplavUtils.adicionarMascara(campoPlaca);
        campoPlaca.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    enviarRequisicao();
                    return true;
                }
                return false;
            }
        });
        this.botaoBuscaManual = rootView.findViewById(R.id.btn_realizar_busca_manual);
        this.botaoBuscaManual.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_realizar_busca_manual:
                enviarRequisicao();
                break;
            default:
                break;
        }
    }

    private void enviarRequisicao() {
        if (this.campoPlaca.getText().toString() != null && !this.campoPlaca.getText().toString().equals("")) {
            if (this.campoPlaca.getText().toString().trim().replace("-", "").length() < 7) {
                Toast.makeText(getActivity(), "Por favor, insira a placa completa", Toast.LENGTH_SHORT).show();
            } else {
                RetornoPlacaService retornoPlacaService = new RetornoPlacaService(getResources().getString(R.string.servidor));
                BuscaRetornoPlacaTask buscaRetornoPlacaTask = new BuscaRetornoPlacaTask(getActivity(), retornoPlacaService, null, this.campoPlaca.getText().toString().trim().replace("-", ""), true);
                buscaRetornoPlacaTask.execute();
            }
        } else {
            Toast.makeText(getActivity(), "Por favor, insira primeiro a placa", Toast.LENGTH_SHORT).show();
        }
    }
}
