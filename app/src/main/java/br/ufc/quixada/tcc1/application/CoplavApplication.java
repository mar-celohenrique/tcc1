package br.ufc.quixada.tcc1.application;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by marcelohenrique on 16/11/17.
 */

public class CoplavApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name("coplav.realm").build();
        Realm.setDefaultConfiguration(config);
    }
}
