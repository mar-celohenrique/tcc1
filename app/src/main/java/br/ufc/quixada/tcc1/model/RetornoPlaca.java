package br.ufc.quixada.tcc1.model;

/**
 * Created by marcelohenrique on 30/09/17.
 */

public class RetornoPlaca {

    private String ano;
    private String anoModelo;
    private String chassi;
    private String cidade;
    private String cor;
    private String data;
    private String marca;
    private String modelo;
    private String placa;
    private String situacao;
    private String uf;
    private Integer codigoSituacao;

    private Identificacao identificacao;

    public Identificacao getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(Identificacao identificacao) {
        this.identificacao = identificacao;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getAnoModelo() {
        return anoModelo;
    }

    public void setAnoModelo(String anoModelo) {
        this.anoModelo = anoModelo;
    }

    public String getChassi() {
        return chassi;
    }

    public void setChassi(String chassi) {
        this.chassi = chassi;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public Integer getCodigoSituacao() {
        return codigoSituacao;
    }

    public void setCodigoSituacao(Integer codigoSituacao) {
        this.codigoSituacao = codigoSituacao;
    }

    @Override
    public String toString() {
        String sb = "Placa: ".concat(placa).concat("\n") +
                "Modelo: ".concat(modelo).concat("\n") +
                "Chassi: ".concat(chassi).concat("\n") +
                "Ano: ".concat(ano).concat("\n") +
                "Ano modelo: ".concat(anoModelo).concat("\n") +
                "Cor: ".concat(cor).concat("\n") +
                "Cidade: ".concat(cidade).concat("\n") +
                "Estado: ".concat(uf).concat("\n");
        return sb;
    }

    public enum Identificacao {
        SUCESSO("sucesso"), INSUCESSO("insucesso");

        private String descricao;

        private Identificacao(String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }
    }
}
