package br.ufc.quixada.tcc1.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeWarningDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;

import br.ufc.quixada.tcc1.R;
import br.ufc.quixada.tcc1.model.DenunciaSinistro;
import br.ufc.quixada.tcc1.service.DenunciaSinistroService;


/**
 * Created by marcelohenrique on 14/11/17.
 */

public class InformarSinistroTask extends AsyncTask<String, Void, DenunciaSinistro> {

    private Context context;
    private DenunciaSinistroService service;
    private DenunciaSinistro denunciaSinistro;
    private AwesomeProgressDialog progress;
    private AwesomeSuccessDialog successDialog;
    private AwesomeWarningDialog warningDialog;

    public InformarSinistroTask(Context context, DenunciaSinistroService service, DenunciaSinistro denunciaSinistro) {
        this.context = context;
        this.service = service;
        this.denunciaSinistro = denunciaSinistro;
        this.progress = new AwesomeProgressDialog(context);
        this.successDialog = new AwesomeSuccessDialog(context);
        this.warningDialog = new AwesomeWarningDialog(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress.setTitle("Aguarde")
                .setMessage("Informando sinistro..")
                .setColoredCircle(R.color.dialogInfoBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .show();
    }

    @Override
    protected DenunciaSinistro doInBackground(String... strings) {
        DenunciaSinistro retorno;
        try {
            retorno = service.denunciar(denunciaSinistro);
        } catch (Exception e) {
            retorno = new DenunciaSinistro();
        }
        return retorno;
    }

    @Override
    protected void onPostExecute(DenunciaSinistro resultado) {
        progress.hide();
        if (null != resultado) {
            exibirMensagemSucesso();
        } else if (null == resultado) {
            exibirMensagemErro();
        } else {
            exibirMensagem("Houve um erro e não foi possível acessar o nosso servidor..");
        }
    }

    private void exibirMensagem(String menssagem) {
        Toast.makeText(context, menssagem, Toast.LENGTH_LONG).show();
    }

    private void exibirMensagemSucesso() {
        successDialog.setTitle("Sucesso!")
                .setMessage("O sinistro informado foi adicionado a nossa base de dados!")
                .setColoredCircle(R.color.dialogSuccessBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_success, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText("OK")
                .setPositiveButtonbackgroundColor(R.color.dialogSuccessBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        successDialog.hide();
                    }
                })
                .show();
    }

    private void exibirMensagemErro() {
        warningDialog.setTitle("Erro!")
                .setMessage("Houve um erro ao informar o sinistro")
                .setColoredCircle(R.color.dialogErrorBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_warning, R.color.white)
                .setCancelable(true)
                .setButtonText("OK")
                .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                .setWarningButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        warningDialog.hide();
                    }
                })
                .show();
    }
}
