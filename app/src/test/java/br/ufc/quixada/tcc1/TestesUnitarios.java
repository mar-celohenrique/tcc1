package br.ufc.quixada.tcc1;

import android.content.Context;
import android.location.Geocoder;
import android.widget.EditText;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;

import br.ufc.quixada.tcc1.application.CoplavApplication;
import br.ufc.quixada.tcc1.model.DenunciaSinistro;
import br.ufc.quixada.tcc1.model.Foto;
import br.ufc.quixada.tcc1.service.DenunciaSinistroService;
import br.ufc.quixada.tcc1.service.RetornoPlacaService;
import br.ufc.quixada.tcc1.util.CoplavUtils;

/**
 * Created by marcelohenrique on 27/11/17.
 */
public class TestesUnitarios {


    private Context context = Mockito.mock(Context.class);

    private EditText editText = Mockito.mock(EditText.class);

    private Geocoder geocoder = Mockito.mock(Geocoder.class);

    private CoplavApplication application = Mockito.mock(CoplavApplication.class);

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private RetornoPlacaService retornoPlacaService = new RetornoPlacaService("http://10.0.126.71:8080/");

    private DenunciaSinistroService denunciaSinistroService = new DenunciaSinistroService("http://10.0.126.71:8080/");


    /**
     * Início dos testes da classe CoplavUtils
     */

    @Test
    public void validarChassiNuloTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("O número do chassi não pode ser nulo");
        CoplavUtils.validarChassi(null);
    }

    @Test
    public void validarChassiMenorDezesseteCaracteresTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("O chassi deve conter 17 caracteres");
        CoplavUtils.validarChassi("9BHBG51CAHP6647");
    }

    @Test
    public void validarChassiInvalidoTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("O número do chassi é inválido");
        CoplavUtils.validarChassi("9BHBGiIoOqQ664779");
    }

    @Test
    public void validarChassiValidoTest() {
        CoplavUtils.validarChassi("9BHBG51CAHP664779");
    }

    @Test
    public void adicionarMascaraEditTextNuloTest() {
        exception.expect(NullPointerException.class);
        exception.expectMessage("O campo de texto não pode ser nulo");
        CoplavUtils.adicionarMascara(null);
    }

    @Test
    public void adicionarMascaraEditTextValidoTest() {
        CoplavUtils.adicionarMascara(editText);
    }

    @Test
    public void salvarDadosNuloTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Devem existir dados a serem persistidos");
        CoplavUtils.salvarDados(null, context);
    }

    @Test
    public void salvarDadosVaziosTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Devem existir dados a serem persistidos");
        CoplavUtils.salvarDados(new ArrayList<DenunciaSinistro>(), context);
    }

    @Test
    public void buscarLocalizacaoPeloEnderecoNuloTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Deve haver um endereço válido a ser buscado");
        CoplavUtils.buscarLocalizacaoPeloEndereco(null, geocoder);
    }

    @Test
    public void buscarLocalizacaoPeloEnderecoInvalidoTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Deve haver um endereço válido a ser buscado");
        CoplavUtils.buscarLocalizacaoPeloEndereco("Ip ", geocoder);
    }

    @Test
    public void buscarLocalizacaoPeloEnderecoValidoTest() {
        CoplavUtils.buscarLocalizacaoPeloEndereco("Rua David Noronha, 5846, Vila Macena - Tabuleiro do Norte, CE", geocoder);
    }

    /**
     * Fim dos testes da classe CoplavUtils
     */


    /**
     * Início dos testes da classe RetornoPlacaService
     */

    @Test
    public void buscaManualPlacaNulaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A placa não deve ser nula");
        retornoPlacaService.buscaManual(null);
    }

    @Test
    public void buscaManualPlacaInvalidaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A placa deve conter 7 caracteres");
        retornoPlacaService.buscaManual("PFW198");
    }

    @Test
    public void buscaManualPlacaValidaTest() {
        retornoPlacaService.buscaManual("PFW1983");
    }

    @Test
    public void buscaPorImagemNulaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A foto não pode ser nula");
        retornoPlacaService.buscaPorImagem(null);
    }

    @Test
    public void buscaPorImagemInvalidaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A foto deve conter nome e base64");
        Foto foto = new Foto();
        foto.setNome("Foto");
        retornoPlacaService.buscaPorImagem(foto);
    }

    @Test
    public void buscaPorImagemValidaTest() {
        Foto foto = new Foto();
        foto.setNome("Foto");
        foto.setBase64("abasfASasfklas");
        retornoPlacaService.buscaPorImagem(foto);
    }

    @Test
    public void instanciarRetornoPlacaServiceURLNulaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A url do servidor não pode ser nula");
        RetornoPlacaService retornoPlacaService = new RetornoPlacaService(null);
    }

    @Test
    public void instanciarRetornoPlacaServiceURLVaziaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A url do servidor não pode ser vazia");
        RetornoPlacaService retornoPlacaService = new RetornoPlacaService("    ");
    }

    @Test
    public void instanciarRetornoPlacaServiceURLValidaTest() {
        RetornoPlacaService retornoPlacaService = new RetornoPlacaService("http://10.0.126.71:8080/");
    }

    /**
     * Fim dos testes da classe RetornoPlacaService
     */


    /**
     * Início dos testes da classe DenunciaSinistroService
     */

    @Test
    public void denunciarPlacaNulaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A placa não deve ser nula");
        DenunciaSinistro denunciaSinistro = new DenunciaSinistro();
        denunciaSinistro.setChassi("9BHBG51CAHP664779");
        denunciaSinistro.setPlaca(null);
        denunciaSinistroService.denunciar(denunciaSinistro);
    }

    @Test
    public void denunciarPlacaInvalidaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A placa deve conter 7 caracteres");
        DenunciaSinistro denunciaSinistro = new DenunciaSinistro();
        denunciaSinistro.setChassi("9BHBG51CAHP664779");
        denunciaSinistro.setPlaca("PFW198");
        denunciaSinistroService.denunciar(denunciaSinistro);
    }

    @Test
    public void denunciarPlacaValidaTest() {
        DenunciaSinistro denunciaSinistro = new DenunciaSinistro();
        denunciaSinistro.setChassi("9BHBG51CAHP664779");
        denunciaSinistro.setPlaca("PFW1983");
        denunciaSinistroService.denunciar(denunciaSinistro);
    }

    @Test
    public void buscarAtualizacaoDataNulaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A data não pode ser nula");
        denunciaSinistroService.buscarAtualizacao(null);
    }

    @Test
    public void buscarAtualizacaoDataValidaTest() {
        denunciaSinistroService.buscarAtualizacao(new Date());
    }

    @Test
    public void buscarSinistrosTest() {
        denunciaSinistroService.buscarSinistros();
    }

    @Test
    public void instanciarDenunciaSinistroServiceURLNulaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A url do servidor não pode ser nula");
        DenunciaSinistroService denunciaSinistroService = new DenunciaSinistroService(null);
    }

    @Test
    public void instanciarDenunciaSinistroServiceURLVaziaTest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("A url do servidor não pode ser vazia");
        DenunciaSinistroService denunciaSinistroService = new DenunciaSinistroService("    ");
    }

    @Test
    public void instanciarDenunciaSinistroServiceURLValidaTest() {
        DenunciaSinistroService denunciaSinistroService = new DenunciaSinistroService("http://10.0.126.71:8080/");
    }

    /**
     * Fim dos testes da classe DenunciaSinistroService
     */

    @Test
    public void iniciarAplicacao(){
        application.onCreate();
    }
}
